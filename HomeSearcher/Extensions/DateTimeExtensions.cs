﻿using System;
using Windows.ApplicationModel.Resources;

namespace HomeSearcher.Extensions
{
    /// <summary>
    /// Provides extensions for the <see cref="DateTime" /> class.
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Gets the relative time.
        /// </summary>
        /// <param name="dateTime">The date time offset.</param>
        /// <returns>The relative time.</returns>
        internal static string ToRelativeTime(this DateTime dateTime)
        {
            var s = DateTime.Now.Subtract(dateTime);
            var resourceLoader = ResourceLoader.GetForCurrentView();

            var dayDifference = (int)s.TotalDays;

            var secondDifference = (int)s.TotalSeconds;

            if (dayDifference < 0)
            {
                return null;
            }

            if (dayDifference == 0)
            {
                if (secondDifference < 60)
                {
                    return resourceLoader.GetString("DateTime_JustNow");
                }

                if (secondDifference < 120)
                {
                    return resourceLoader.GetString("DateTime_OneMinute");
                }

                if (secondDifference < 3600)
                {
                    return string.Format(resourceLoader.GetString("DateTime_MinutesAgo"), secondDifference / 60);
                }

                if (secondDifference < 7200)
                {
                    return resourceLoader.GetString("DateTime_OneHour");
                }

                if (secondDifference < 86400)
                {
                    return string.Format(resourceLoader.GetString("DateTime_HoursAgo"), secondDifference / 3600);
                }
            }

            if (dayDifference == 1)
            {
                return resourceLoader.GetString("DateTime_Yesterday");
            }

            if (dayDifference < 7)
            {
                return String.Format(resourceLoader.GetString("DateTime_DaysAgo"), dayDifference);
            }

            if (dayDifference < 14)
            {
                return resourceLoader.GetString("DateTime_OneWeekAgo");
            }

            if (dayDifference < 31)
            {
                return String.Format(resourceLoader.GetString("DateTime_WeeksAgo"), dayDifference / 7);
            }

            if (dayDifference < 62)
            {
                return resourceLoader.GetString("DateTime_OneMonthAgo");
            }

            return String.Format(resourceLoader.GetString("DateTime_MonthsAgo"), dayDifference / 31);
        }
    }
}
