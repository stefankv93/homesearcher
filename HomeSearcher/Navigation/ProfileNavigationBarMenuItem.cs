﻿using HomeSearcher.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace HomeSearcher.Navigation
{
    public class ProfileNavigationBarMenuItem : NavigationBarMenuItemBase, INavigationBarMenuItem
    {
        /// <summary>
        /// Gets the type of the destination page.
        /// </summary>
        public Type DestPage
        {
            get { return typeof(ProfileViewPage); }
        }

        /// <summary>
        /// Gets the title displayed in the navigation bar.
        /// </summary>
        public string Label
        {
            get
            {
                //var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                //return loader.GetString("SettingsNavigationBar_Label");
                return "Profile";
            }
        }

        /// <summary>
        /// Gets the positions of the current item in the navigation bar.
        /// </summary>
        public override NavigationBarItemPosition Position
        {
            get { return NavigationBarItemPosition.Bottom; }
        }

        /// <summary>
        /// Gets the symbol that is displayed in the navigation bar.
        /// </summary>
        public override Symbol Symbol
        {
            get { return Symbol.Account; }
        }
    }
}
