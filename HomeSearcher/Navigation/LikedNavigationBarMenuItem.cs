﻿using HomeSearcher.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace HomeSearcher.Navigation
{
    class LikedNavigationBarMenuItem : NavigationBarMenuItemBase, INavigationBarMenuItem
    {
        public override object Arguments
        {
            get { return "Like"; }
        }

        /// <summary>
        /// Gets the type of the destination page.
        /// </summary>
        public Type DestPage
        {
            get { return typeof(EstatesViewPage); }
        }

        /// <summary>
        /// Gets the title displayed in the navigation bar.
        /// </summary>
        public string Label
        {
            get
            {
                //var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                //return loader.GetString("SettingsNavigationBar_Label");
                return "Liked";
            }
        }

        /// <summary>
        /// Gets the positions of the current item in the navigation bar.
        /// </summary>
        public override NavigationBarItemPosition Position
        {
            get { return NavigationBarItemPosition.Top; }
        }

        /// <summary>
        /// Gets the symbol that is displayed in the navigation bar.
        /// </summary>
        public override Symbol Symbol
        {
            get { return Symbol.Like; }
        }
    }
}
