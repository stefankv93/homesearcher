﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomeSearcher.Views;
using Windows.UI.Xaml.Controls;

namespace HomeSearcher.Navigation
{
    public class EstatesNavigationBarMenuItem : NavigationBarMenuItemBase, INavigationBarMenuItem
    {
        /// <summary>
        /// Gets the arguments that can be passed optionally to the target page.
        /// </summary>
        public override object Arguments
        {
            get { return "None"; }
        }
        /// <summary>
        /// Gets the type of the destination page.
        /// </summary>
        public Type DestPage
        {
            get { return typeof(EstatesViewPage); }
        }

        /// <summary>
        /// Gets the title displayed in the navigation bar.
        /// </summary>
        public string Label
        {
            get
            {
                //var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                //return loader.GetString("SettingsNavigationBar_Label");
                return "Estates";
            }
        }

        /// <summary>
        /// Gets the positions of the current item in the navigation bar.
        /// </summary>
        public override NavigationBarItemPosition Position
        {
            get { return NavigationBarItemPosition.Top; }
        }

        /// <summary>
        /// Gets the symbol that is displayed in the navigation bar.
        /// </summary>
        public override Symbol Symbol
        {
            get { return Symbol.Home; }
        }
    }
}
