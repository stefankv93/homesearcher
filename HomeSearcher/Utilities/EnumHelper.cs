﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Utilities
{
    public class EnumHelper
    {
        public static string ConvertFromEstateType(RealEstate.EstateType estateType)
        {
            switch (estateType)
            {
                case RealEstate.EstateType.None: return "None";
                case RealEstate.EstateType.Apartment: return "Apartment";
                case RealEstate.EstateType.ApartmentInHouse: return "Apartment in a house";
                case RealEstate.EstateType.House: return "House";
                case RealEstate.EstateType.OfficeSpace: return "Office space";
                case RealEstate.EstateType.Shop: return "Shop";
            }

            return "";
        }

        public static RealEstate.EstateType ConvertToEstateType(string estateType)
        {
            switch (estateType)
            {
                case "None" : return RealEstate.EstateType.None;
                case "Apartment": return RealEstate.EstateType.Apartment;
                case "Apartment in house": return RealEstate.EstateType.ApartmentInHouse;
                case "House": return RealEstate.EstateType.House;
                case "Office space": return RealEstate.EstateType.OfficeSpace;
                case "Shop": return RealEstate.EstateType.Shop;
            }

            return RealEstate.EstateType.None;
        }

        public static RealEstate.Heating ConvertToHeatingType(string heatingType)
        {
            switch (heatingType)
            {
                case "None": return RealEstate.Heating.None;
                case "Central": return RealEstate.Heating.Central;
                case "Electric Power": return RealEstate.Heating.ElectricPower;
                case "Floor": return RealEstate.Heating.Floor;
                case "Storage Heater": return RealEstate.Heating.StorageHeater;
                case "Underfloor": return RealEstate.Heating.Underfloor;
            }

            return RealEstate.Heating.None;
        }
    }
}
