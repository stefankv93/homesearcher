﻿using HomeSearcher.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Globalization;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using HomeSearcher.Constants;

namespace HomeSearcher
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();

            // Register for events
            Suspending += OnSuspending;
            UnhandledException += OnUnhandledException;

            TransferToStorage();
            TransferUserPhotos();
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            var shell = Initialize();

            // Place our app shell in the current Window
            Window.Current.Content = shell;

            if (shell.AppFrame.Content == null)
            {
                //var facade = //ServiceLocator.Current.GetInstance<INavigationFacade>();

                //facade.NavigateToWelcomeView();
                shell.AppFrame.Navigate(typeof(SignInPage));
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when the application is activated by some means other than normal launching.
        /// </summary>
        /// <param name="args">Event data for the event.</param>
        protected override void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);

            var shell = Window.Current.Content as AppShell;

            // Check if the application needs to be initialized.
            if (shell == null)
            {
                shell = Initialize();
            }

            // Place our app shell in the current Window
            Window.Current.Content = shell;
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        /// <summary>
        /// Invoked when the app runs into an unhandled exception. Telemetry logs the
        /// unhandled exception and appropriate message is displayed to the user experiencing
        /// the exception.
        /// </summary>
        /// <param name="sender">The source of the unhandled exception.</param>
        /// <param name="e">Details about the unhandled exception event.</param>
        private async void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            var resourceLoader = ResourceLoader.GetForCurrentView();
            var dialog = new MessageDialog(resourceLoader.GetString("UnexpectedError_Message"),
                resourceLoader.GetString("UnexpectedError_Title"));

            await dialog.ShowAsync();
        }

        /// <summary>
        /// Initialize the App launch.
        /// </summary>
        /// <returns>The AppShell of the app.</returns>
        private AppShell Initialize()
        {
            var shell = Window.Current.Content as AppShell;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (shell == null)
            {
                // Create a AppShell to act as the navigation context and navigate to the first page
                shell = new AppShell();

                // Set the default language
                shell.Language = ApplicationLanguages.Languages[0];

                shell.AppFrame.NavigationFailed += OnNavigationFailed;
            }

            return shell;
        }

        private async void TransferToStorage()
        {
            //Ensure that we have file in local folder for xml data
            Windows.Storage.StorageFolder storageFolder =
                 Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.ApplicationModel.Package.Current.InstalledLocation;
            Windows.Storage.StorageFolder destinationFolder = (Windows.Storage.StorageFolder) await storageFolder.TryGetItemAsync(Constants.Constants.LocalStorageFolderName);

            if(destinationFolder == null)
            {
                destinationFolder = await storageFolder.CreateFolderAsync(Constants.Constants.LocalStorageFolderName);
            }

            Windows.Storage.StorageFile userFile = (Windows.Storage.StorageFile) await destinationFolder.TryGetItemAsync(Constants.Constants.LocalStorageUserFile);

            if(userFile != null)
            {
                return;
            }

            Windows.Storage.StorageFolder sourceFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            Windows.Storage.StorageFile sourceFile = await sourceFolder.GetFileAsync(Constants.Constants.LocalStorageFolderName + "\\" + Constants.Constants.LocalStorageUserFile);

            await sourceFile.CopyAsync(destinationFolder);
        }

        private async void TransferUserPhotos()
        {
            //Ensure that we have file in local folder for xml data
            Windows.Storage.StorageFolder storageFolder =
                 Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.ApplicationModel.Package.Current.InstalledLocation;
            Windows.Storage.StorageFolder destinationFolder = (Windows.Storage.StorageFolder)await storageFolder.TryGetItemAsync(Constants.Constants.LocalUserPhotosFolderName);

            if (destinationFolder == null)
            {
                destinationFolder = await storageFolder.CreateFolderAsync(Constants.Constants.LocalUserPhotosFolderName);

                Windows.Storage.StorageFolder sourceFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                sourceFolder = await sourceFolder.GetFolderAsync("Assets");
                sourceFolder = await sourceFolder.GetFolderAsync("UserPhotos");

                foreach (var sourceFile in await sourceFolder.GetFilesAsync())
                {
                    await sourceFile.CopyAsync(destinationFolder);
                }
            }

            //Windows.Storage.StorageFile sourceFile = await sourceFolder.GetFileAsync(Constants.Constants.LocalStorageFolderName + "\\" + Constants.Constants.LocalStorageUserFile);

            //await sourceFile.CopyAsync(destinationFolder);
        }
    }
}
