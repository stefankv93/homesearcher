﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace HomeSearcher.Controls
{
    public class NavMenuListView : ListView
    {
        private SplitView _splitViewHost;

        public NavMenuListView()
        {
            SelectionMode = ListViewSelectionMode.Single;
            IsItemClickEnabled = true;
            ItemClick += ItemClickedHandler;

            Loaded += (s, a) =>
            {
                // Locate the hosting SplitView control
                var parent = VisualTreeHelper.GetParent(this);

                while (parent != null && !(parent is SplitView))
                {
                    parent = VisualTreeHelper.GetParent(parent);
                }

                if (parent != null)
                {
                    _splitViewHost = parent as SplitView;

                    _splitViewHost.RegisterPropertyChangedCallback(SplitView.IsPaneOpenProperty,
                        (sender, args) => { OnPaneToggled(); });

                    // Call once to ensure we're in the correct state
                    OnPaneToggled();
                }
            };
        }

        /// <summary>
        /// Occurs when an item has been selected
        /// </summary>
        public event EventHandler<ListViewItem> ItemInvoked;

        /// <summary>
        /// Mark the <paramref name="item" /> as selected and ensures everything else is not.
        /// If the <paramref name="item" /> is null then everything is unselected.
        /// </summary>
        /// <param name="item">The item to select.</param>
        public void SetSelectedItem(ListViewItem item)
        {
            var index = -1;

            if (item != null)
            {
                index = IndexFromContainer(item);
            }

            for (var i = 0; i < Items.Count; i++)
            {
                var listViewItem = (ListViewItem)ContainerFromIndex(i);

                if (listViewItem != null)
                {
                    if (i != index)
                    {
                        listViewItem.IsSelected = false;
                    }
                    else if (i == index)
                    {
                        listViewItem.IsSelected = true;
                    }
                }
            }
        }

        private void ItemClickedHandler(object sender, ItemClickEventArgs e)
        {
            // Triggered when the item is selected using 
            // something other than a keyboard
            var item = ContainerFromItem(e.ClickedItem);
            InvokeItem(item);
        }

        private void InvokeItem(object focusedItem)
        {
            SetSelectedItem(focusedItem as ListViewItem);
            ItemInvoked?.Invoke(this, focusedItem as ListViewItem);

            if (_splitViewHost.IsPaneOpen && (
                _splitViewHost.DisplayMode == SplitViewDisplayMode.CompactOverlay ||
                _splitViewHost.DisplayMode == SplitViewDisplayMode.Overlay))
            {
                _splitViewHost.IsPaneOpen = false;
                if (focusedItem is ListViewItem)
                {
                    ((ListViewItem)focusedItem).Focus(FocusState.Programmatic);
                }
            }
        }

        /// <summary>
        /// Re-size the ListView's Panel when the SplitView is compact so the items
        /// will fit within the visible space and correctly display a keyboard focus rect.
        /// </summary>
        private void OnPaneToggled()
        {
            if (_splitViewHost.IsPaneOpen)
            {
                ItemsPanelRoot.ClearValue(WidthProperty);
                ItemsPanelRoot.ClearValue(HorizontalAlignmentProperty);
            }
            else if (_splitViewHost.DisplayMode == SplitViewDisplayMode.CompactInline ||
                     _splitViewHost.DisplayMode == SplitViewDisplayMode.CompactOverlay)
            {
                ItemsPanelRoot.SetValue(WidthProperty, _splitViewHost.CompactPaneLength);
                ItemsPanelRoot.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
            }
        }
    }
}
