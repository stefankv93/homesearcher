﻿using HomeSearcher.Commands;
using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace HomeSearcher.Controls
{
    public sealed partial class ToggledButton : UserControl, INotifyPropertyChanged
    {
        public ToggledButton()
        {
            this.InitializeComponent();
        }

        public bool IsPressed
        {
            get { return (bool)GetValue(IsPressedProperty); }
            set { SetValue(IsPressedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPressed.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPressedProperty =
            DependencyProperty.Register("IsPressed", typeof(bool), typeof(ToggledButton), null);

        public RelayCommand<RealEstate> ButtonPressCommand
        {
            get { return (RelayCommand<RealEstate>)GetValue(ButtonPressCommandProperty); }
            set { SetValue(ButtonPressCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonPressCommandProperty =
            DependencyProperty.Register("ButtonPressCommand", typeof(RelayCommand<RealEstate>), typeof(ToggledButton), null);

        public RealEstate ButtonCommandParameter
        {
            get { return (RealEstate)GetValue(ButtonCommandParameterProperty); }
            set { SetValue(ButtonCommandParameterProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ButtonCommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonCommandParameterProperty =
            DependencyProperty.Register("ButtonCommandParameter", typeof(RealEstate), typeof(ToggledButton), null);


        /// <summary>
        /// Notifies that the property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
