﻿using HomeSearcher.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace HomeSearcher.Controls
{
    public sealed partial class TextBoxWithIcon : UserControl, INotifyPropertyChanged
    {
        public TextBoxWithIcon()
        {
            this.InitializeComponent();
            _values = new List<bool> { false, false };
            _values = GetTextBoxBindingElements();
            txtUserEntry.GotFocus += TxtUserEntryEvents;
            txtUserEntry.LostFocus += TxtUserEntryEvents;
            txtUserEntry.TextChanging += TxtUserEntry_TextChanging;
            passwordUserEntry.GotFocus += TxtUserEntryEvents;
            passwordUserEntry.LostFocus += TxtUserEntryEvents;
            passwordUserEntry.PasswordChanged += PasswordUserEntry_TextChanging;
        }

        private void PasswordUserEntry_TextChanging(object sender, RoutedEventArgs e)
        {
            Values = GetTextBoxBindingElements();
        }

        private void TxtUserEntry_TextChanging(TextBox sender, TextBoxTextChangingEventArgs args)
        {
            Values = GetTextBoxBindingElements();
        }

        private void TxtUserEntryEvents(object sender, RoutedEventArgs e)
        {
            Values = GetTextBoxBindingElements();
        }

        private List<bool> GetTextBoxBindingElements()
        {
            bool firtBool = IsPassword ? passwordUserEntry.Password.Length == 0 : txtUserEntry.Text.Length == 0;

            bool secondBool = IsPassword ? passwordUserEntry.FocusState != FocusState.Unfocused : txtUserEntry.FocusState != FocusState.Unfocused;

            var list = new List<bool>() { firtBool, secondBool };

            if (list[0] != _values[0]) return list;

            if (list[1] != _values[1]) return list;

            return _values;
        }

        private List<bool> _values;

        public List<bool> Values
        {
            get
            {
                return _values;
                //return new List<bool>() { txtUserEntry.Text.Length == 0, txtUserEntry.FocusState != FocusState.Unfocused };
            }
            set
            {
                if(value != _values)
                {
                    _values = value;
                    NotifyPropertyChanged(nameof(Values));
                }
            }
        }

        public string DynamicText
        {
            get { return (string)GetValue(DynamicTextProperty); }
            set{ SetValue(DynamicTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DynamicText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DynamicTextProperty =
            DependencyProperty.Register("DynamicText", typeof(string), typeof(TextBoxWithIcon), null);
        
        public string PlaceholderText
        {
            get { return txtPlaceholderEntry.Text; }
            set { txtPlaceholderEntry.Text = value; }
        }

        // Using a DependencyProperty as the backing store for PlaceholderText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceholderTextProperty =
            DependencyProperty.Register("PlaceholderText", typeof(string), typeof(TextBoxWithIcon), null);



        public ImageSource IconSource
        {
            get { return (ImageSource)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSourceProperty =
            DependencyProperty.Register("IconSource", typeof(ImageSource), typeof(TextBoxWithIcon), null);



        public bool IsPassword
        {
            get { return (bool)GetValue(IsPasswordProperty); }
            set { SetValue(IsPasswordProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsPassword.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsPasswordProperty =
            DependencyProperty.Register("IsPassword", typeof(bool), typeof(TextBoxWithIcon), null);



        /// <summary>
        /// Notifies that the property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
