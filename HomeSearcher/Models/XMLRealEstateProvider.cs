﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeSearcher.Models
{
    public class XMLRealEstateProvider : IRealEstateProvider
    {
        public string FileName
        {
            get; set;
        }

        public List<RealEstate> GetRealEstates()
        {
            if (string.IsNullOrEmpty(FileName))
            {
                throw new Exception("Empty or null file name for deserialization");
            }

            // Create sample file; replace if exists.
            Windows.Storage.StorageFolder storageFolder =
                 Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.ApplicationModel.Package.Current.InstalledLocation;
            Windows.Storage.StorageFile sampleFile =
                    storageFolder.GetFileAsync(FileName).AsTask().GetAwaiter().GetResult();

            List<RealEstate> deserializedRealEstates = new List<RealEstate>();

            using (var stream = sampleFile.OpenStreamForReadAsync().Result)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<RealEstate>));
                deserializedRealEstates = (List<RealEstate>)serializer.Deserialize(stream);
            }

            return deserializedRealEstates;
        }

        public void SetRealEstates(List<RealEstate> realEstates)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                throw new Exception("Empty or null file name for deserialization");
            }

            // Create sample file; replace if exists.
            Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile =
                storageFolder.CreateFileAsync(FileName,
                    Windows.Storage.CreationCollisionOption.ReplaceExisting).AsTask().GetAwaiter().GetResult();

            using (var stream = sampleFile.OpenStreamForWriteAsync().Result)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<RealEstate>));
                serializer.Serialize(stream, realEstates);
            }
        }
    }
}
