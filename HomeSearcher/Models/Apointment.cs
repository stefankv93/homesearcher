﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public class Apointment
    {
        private DateTime _appointmentDate;
        public DateTime AppointmentDate
        {
            get { return _appointmentDate; }
            set
            {
                _appointmentDate = value;
            }
        }

        private RealEstate _realEstate;
        public RealEstate RealEstate
        {
            get { return _realEstate; }
            set
            {
                _realEstate = value;
            }
        }
    }
}
