﻿namespace HomeSearcher.Models
{
    public class Admin : AbstractUser
    {
        public override Role UserRole { get { return Role.Admin; } }

        public Admin()
            : base(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty)
        {

        }

        public Admin(string username, string password, string name, string surname, string email, string phone, string address)
            : base(username, password, name, surname, email, phone, address)
        {
        }
    }
}
