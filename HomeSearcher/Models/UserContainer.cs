﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public class UserContainer
    {
        Dictionary<string, AbstractUser> usersDictionary = new Dictionary<string, AbstractUser>();
        IUserProvider userProvider = null;

        public UserContainer(IUserProvider userProvider)
        {
            this.userProvider = userProvider;
            List<AbstractUser> users = userProvider.GetUsers();
            foreach (var user in users)
            {
                if (!usersDictionary.ContainsKey(user.UserName))
                {
                    usersDictionary.Add(user.UserName, user);
                }
                else
                {
                    throw new Exception(string.Format("User with the username: {0} is already registered to the system", user.UserName));
                }
            }
        }

        public bool AddUser(AbstractUser user)
        {
            if (usersDictionary.ContainsKey(user.UserName))
            {
                return false;
            }
            else
            {
                usersDictionary[user.UserName] = user;
                userProvider.SetUsers(usersDictionary.Values.ToList());
                return true;
            }
        }

        public AbstractUser GetUser(string userName)
        {
            if (!usersDictionary.ContainsKey(userName))
            {
                return null;
            }
            else
            {
                return usersDictionary[userName];
            }
        }

        public List<AbstractUser> GetAdmins()
        {
            List<AbstractUser> admins = new List<AbstractUser>();
            foreach (var user in usersDictionary)
            {
                if (user.Value.UserRole == User.Role.Admin)
                {
                    admins.Add(user.Value);
                }
            }

            return admins;
        }

        public List<AbstractUser> GetUsers()
        {
            List<AbstractUser> buyers = new List<AbstractUser>();
            foreach (var user in usersDictionary)
            {
                if (user.Value.UserRole == User.Role.User)
                {
                    buyers.Add(user.Value);
                }
            }

            return buyers;
        }
    }
}
