﻿using HomeSearcher.ComponentModel;
using HomeSearcher.Facades;
using HomeSearcher.ViewModels;
using HomeSearcher.Views;
using System;

namespace HomeSearcher.Models
{
    /// <summary>
    /// Contains global access to specific data.
    /// </summary>
    public class ApplicationEnviroment : ObservableObjectBase
    {
        public ApplicationEnviroment()
        {
            _currentUser = null;
            _userContainer = new UserContainer(new XMLUserProvider() { FileName = "Files\\users.xml" });
            _realEstateContainer = new RealEstatesContainer(new RandomRealEstateProvider(60, _userContainer.GetUsers()));
            _navigationFacade = new NavigationFacade();

            NavigationFacade.AddType(typeof(EstatesViewPage), typeof(EstatesViewModel));
            NavigationFacade.AddType(typeof(EstateViewDetailsPage), typeof(EstateDetailsViewModel));
        }

        private static ApplicationEnviroment _appEnviroment;

        public static ApplicationEnviroment Instance
        {
            get
            {
                if(_appEnviroment == null)
                {
                    _appEnviroment = new ApplicationEnviroment();
                }

                return _appEnviroment;
            }
        }

        private NavigationFacade _navigationFacade;

        public NavigationFacade NavigationFacade
        {
            get { return _navigationFacade; }
        }

        private AbstractUser _currentUser;

        /// <summary>
        /// Stores the current user that is logged in.
        /// </summary>
        public AbstractUser CurrentUser
        {
            get { return _currentUser; }
            set
            {
                if (value != _currentUser)
                {
                    _currentUser = value;
                    NotifyPropertyChanged(nameof(CurrentUser));

                    CurrentUserChanged?.Invoke(this, CurrentUser);
                }
            }
        }

        private UserContainer _userContainer;

        public UserContainer UserContainer
        {
            get { return _userContainer;  }
            private set { _userContainer = value; }
        }

        private RealEstatesContainer _realEstateContainer;

        public RealEstatesContainer RealEstatesContainer
        {
            get { return _realEstateContainer; }
            set { _realEstateContainer = value; }
        }

        /// <summary>
        /// Invoked when the current user has changed.
        /// </summary>
        public event EventHandler<AbstractUser> CurrentUserChanged;



        
    }
}
