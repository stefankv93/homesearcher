﻿using HomeSearcher.ComponentModel;
using System.Xml.Serialization;

namespace HomeSearcher.Models
{
    [XmlInclude(typeof(Admin))]
    [XmlInclude(typeof(User))]
    public abstract class AbstractUser : ObservableObjectBase
    {
        public enum Role { Admin, User, Agent };

        protected AbstractUser(string userName, string password, string name, string surname, string email, string phone, string address)
        {
            this.UserName = userName;
            this.Password = password;
            this.Name = name;
            this.Surname = surname;
            this.Email = email;
            this.Phone = phone;
            this.Address = address;
        }

        public abstract Role UserRole
        {
            get;
        }

        private string _username;
        public string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                if(_username != value)
                {
                    _username = value;
                    NotifyPropertyChanged(nameof(UserName));
                }
            }
        }

        public string Password
        {
            get; set;
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    NotifyPropertyChanged(nameof(Name));
                }
            }
        }

        private string _surname;
        public string Surname
        {
            get
            {
                return _surname;
            }
            set
            {
                if (_surname != value)
                {
                    _surname = value;
                    NotifyPropertyChanged(nameof(Surname));
                }
            }
        }

        private string _email;
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    NotifyPropertyChanged(nameof(Email));
                }
            }
        }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                if (_phone != value)
                {
                    _phone = value;
                    NotifyPropertyChanged(nameof(Phone));
                }
            }
        }

        private string _address;
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                if (_address != value)
                {
                    _address = value;
                    NotifyPropertyChanged(nameof(Address));
                }
            }
        }

        private string _imageLocation;
        public string ImageLocation
        {
            get
            {
                return string.Format(@"ms-appdata:///local/UserPhotos/{0}.jpg", _username);
            }
            set
            {
                if (_imageLocation != value)
                {
                    _imageLocation = value;
                    NotifyPropertyChanged(nameof(ImageLocation));
                }
            }
        }
    }
}
