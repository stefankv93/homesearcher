﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public interface IRealEstateProvider
    {
        List<RealEstate> GetRealEstates();
        void SetRealEstates(List<RealEstate> realEstaets);
    }
}
