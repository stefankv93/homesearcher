﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public class RealEstatesContainer
    {
        public List<RealEstate> realEstates = new List<RealEstate>();
        IRealEstateProvider realEstateProvider = null;

        public RealEstatesContainer(IRealEstateProvider realEstateProvider)
        {
            this.realEstateProvider = realEstateProvider;

            realEstates = realEstateProvider.GetRealEstates();
        }

        public List<RealEstate> GetRealEstates()
        {
            return realEstates;
        }

        public void AddRealEstate(RealEstate estate)
        {
            realEstates.Add(estate);
            realEstateProvider.SetRealEstates(realEstates);
        }

        public void RemoveEstate(int id)
        {
            var estateToRemove = realEstates.Find((estate) => estate.Id == id);
            realEstates.Remove(estateToRemove);
            realEstateProvider.SetRealEstates(realEstates);
        }
    }
}
