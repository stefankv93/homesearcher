﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public class Comment
    {
        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public string UserImage
        {
                get
            {
                    return string.Format(@"ms-appdata:///local/UserPhotos/{0}.jpg", _username);
                }
            set { _username = value; }
        }

        private string _content;

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        private bool _isPending;

        public bool IsPending
        {
            get { return _isPending; }
            set { _isPending = value; }
        }


    }
}
