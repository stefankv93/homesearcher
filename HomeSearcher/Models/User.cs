﻿using System.Collections.Generic;

namespace HomeSearcher.Models
{
    public class User : AbstractUser
    {
        public override Role UserRole { get { return Role.User; } }

        public User()
            : base(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty)
        {

        }

        public User(string username, string password, string name, string surname, string email, string phone, string address)
            : base(username, password, name, surname, email, phone, address)
        {
        }

        List<Apointment> _apointments = new List<Apointment>();
        public List<Apointment> Apointments
        {
            get { return _apointments; }
            set
            {
                _apointments = value;
            }
        }
    }
}
