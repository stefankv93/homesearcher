﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public interface IUserProvider
    {
        List<AbstractUser> GetUsers();
        void SetUsers(List<AbstractUser> users);
    }
}