﻿using System;
using System.Collections.Generic;
using HomeSearcher.ComponentModel;
using Windows.UI.Xaml.Media;
using HomeSearcher.Lifecycle;
using System.Linq;

namespace HomeSearcher.Models
{

    public class RealEstate : ObservableObjectBase
    {
        public RealEstate()
        {
            _id = EstateIdProvider.GetRealEstateId();
        }

        private int _id = 0;
        public int Id
        {
            get
            {
                return _id;
            }
        }

        private string _location;
        public string Location
        {
            get
            {
                return _location;
            }
            set
            {
                if (_location != value)
                {
                    _location = value;
                    NotifyPropertyChanged(nameof(Location));
                }
            }
        }

        public enum EstateType { None, Apartment, House, ApartmentInHouse, Shop, OfficeSpace };

        private EstateType _type;
        public EstateType Type
        {
            get { return _type; }
            set
            {
                if (value != _type)
                {
                    _type = value;
                    NotifyPropertyChanged(nameof(Type));
                }
            }
        }

        public enum EstateSaleType { None, ForSale, ForRent };
        private EstateSaleType _saletType;
        public EstateSaleType SaleType
        {
            get { return _saletType; }
            set
            {
                if (_saletType != value)
                {
                    _saletType = value;
                    NotifyPropertyChanged(nameof(SaleType));
                }
            }
        }

        private double _price;
        public double Price
        {
            get { return _price; }
            set
            {
                if (value != _price)
                {
                    _price = value;
                    NotifyPropertyChanged(nameof(Price));
                }
            }
        }

        private int _area;
        public int Area
        {
            get { return _area; }
            set
            {
                if (value != _area)
                {
                    _area = value;
                    NotifyPropertyChanged(nameof(Area));
                }
            }
        }

        private int _rooms;
        public int Rooms
        {
            get { return _rooms; }
            set
            {
                if (value != _rooms)
                {
                    _rooms = value;
                    NotifyPropertyChanged(nameof(Rooms));
                }
            }
        }

        private int _floor;
        public int Floor
        {
            get { return _floor; }
            set
            {
                if (value != _floor)
                {
                    _floor = value;
                    NotifyPropertyChanged(nameof(Floor));
                }
            }
        }

        public enum Heating { None, Central, Floor, StorageHeater, ElectricPower, Underfloor };

        private Heating _heatingType;
        public Heating HeatingType
        {
            get { return _heatingType; }
            set
            {
                if (value != _heatingType)
                {
                    _heatingType = value;
                    NotifyPropertyChanged(nameof(HeatingType));
                }
            }
        }

        private bool _hasTerrace;
        public bool HasTerrace
        {
            get { return _hasTerrace; }
            set
            {
                if (value != _hasTerrace)
                {
                    _hasTerrace = value;
                    NotifyPropertyChanged(nameof(HasTerrace));
                }
            }
        }

        private bool _isRegistered;
        public bool IsRegistered
        {
            get { return _isRegistered; }
            set
            {
                if (value != _isRegistered)
                {
                    _isRegistered = value;
                    NotifyPropertyChanged(nameof(IsRegistered));
                }
            }
        }

        private DateTime _dateCreation;
        public DateTime DateCreation
        {
            get { return _dateCreation; }
            set
            {
                if (_dateCreation != value)
                {
                    _dateCreation = value;
                    NotifyPropertyChanged(nameof(DateCreation));
                }
            }
        }

        private string _content;
        public string Content
        {
            get { return _content; }
            set
            {
                if (_content != value)
                {
                    _content = value;
                    NotifyPropertyChanged(nameof(Content));
                }
            }
        }

        private List<string> _images;
        public List<string> Images
        {
            get
            {
                if (_images == null || _images.Count == 0)
                {
                    return new List<string>() { @"ms-appx:///Assets/Wide310x150Logo.scale-200.png" };
                }

                return _images;
            }
            set
            {
                if (_images != value)
                {
                    _images = value;
                    NotifyPropertyChanged(nameof(SummaryImageSource));
                    NotifyPropertyChanged(nameof(Images));
                }
            }
        }

        public string SummaryImageSource
        {
            get
            {
                if (_images == null || _images.Count == 0)
                {
                    return @"ms-appx:///Assets/Wide310x150Logo.scale-200.png";
                }

                return _images[0];
            }
        }

        private List<Comment> _comments;
        public List<Comment> Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                if (_comments != value)
                {
                    _comments = value;
                    NotifyPropertyChanged(nameof(HighhlightedComment));
                    NotifyPropertyChanged(nameof(Comments));
                }
            }
        }

        public Comment HighhlightedComment
        {
            get
            {
                if (_comments == null || _comments.Count == 0)
                {
                    return null;
                }

                return _comments[0];
            }
        }

        private List<String> _likes;
        public List<String> Likes
        {
            get { return _likes; }
            set
            {
                if(_likes != value)
                {
                    _likes = value;
                    NotifyPropertyChanged(nameof(Likes));
                    IsLikedByCurrentUser = true;
                }
            }
        }

        public void RemoveUserLike(string user)
        {
            _likes.Remove(user);
            NotifyPropertyChanged(nameof(Likes));
            IsLikedByCurrentUser = true;
        }

        public void AddUserLike(string user)
        {
            _likes.Add(user);
            NotifyPropertyChanged(nameof(Likes));
            IsLikedByCurrentUser = true;
        }

        public bool IsLikedByCurrentUser
        {
            get
            {
                if (_likes == null || _likes.Count == 0) return false;
                if(_likes.Any((user) => user.Equals(ApplicationEnviroment.Instance.CurrentUser.UserName)))
                {
                    return true;
                }

                return false;
            }
            set
            {
                NotifyPropertyChanged(nameof(IsLikedByCurrentUser));
            }
        }
    }
}
