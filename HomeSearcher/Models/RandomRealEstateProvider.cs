﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Models
{
    public class RandomRealEstateProvider: IRealEstateProvider
    {
        private List<RealEstate> _estates = null;
        private List<AbstractUser> _users = null;
        private int _count = 0;

        public RandomRealEstateProvider(int count, List<AbstractUser> users)
        {
            _count = count;
            _users = users;
        }

        public List<RealEstate> GetRealEstates()
        {
            if(_estates == null)
            {
                _estates = new List<RealEstate>();
                for(int i =0; i<_count; i++)
                {
                    var comments = new List<Comment>();
                    var likes = new List<string>();
                    foreach (var user in _users)
                    {
                        int rndCube = rnd.Next(10);
                        if(rndCube > 4)
                        {
                            comments.Add(new Comment()
                            {
                                Username = user.UserName,
                                Content = "Very nice place to stay and rest",
                                IsPending = false
                            });
                        }

                        rndCube = rnd.Next(10);
                        if(rndCube < 6)
                        {
                            likes.Add(user.UserName);
                        }
                    }

                    var images = new List<string>();
                    int cnt = 3 + rnd.Next(3);
                    for (int j = 0; j< cnt; j++)
                    {
                        images.Add("ms-appx:///Assets/RealEstateImages/estate_" + GetRandomInt(1, 20) + ".jpg");
                    }

                    var estate = new RealEstate()
                    {
                        Location = "Sample Location " + i,
                        DateCreation = GetRandomDate(new DateTime(2017, 1, 1), DateTime.Now),
                        Comments = comments,
                        Content = @"This house have beautiful view on a Mediterian sea. The place is ideal for artist beacuse nature is inspiring. Large widnows invite all good vibes inside. The whole voluem is vibrating. Wonderfull!",
                        Area = GetRandomInt(15, 120),
                        Rooms = GetRandomInt(1, 8),
                        Floor = GetRandomInt(0, 100),
                        Price = GetRandomInt(5, 100000),
                        Type = GetRandomEnumValue<RealEstate.EstateType>(),
                        SaleType = GetRandomEnumValue<RealEstate.EstateSaleType>(),
                        HeatingType = GetRandomEnumValue<RealEstate.Heating>(),
                        HasTerrace = rnd.NextDouble() > 0.5 ? true : false,
                        IsRegistered = rnd.NextDouble() > 0.5 ? true : false,
                        Images = images,
                        Likes = likes
                    };

                    _estates.Add(estate);
                }
            }

            return _estates;
        }

        public void SetRealEstates(List<RealEstate> realEstaets)
        {
            _estates = realEstaets;
        }

        static readonly Random rnd = new Random();
        public static DateTime GetRandomDate(DateTime from, DateTime to)
        {
            var range = to - from;

            var randTimeSpan = new TimeSpan((long)(rnd.NextDouble() * range.Ticks));

            return from + randTimeSpan;
        }

        public static int GetRandomInt(int from, int to)
        {
            var range = to - from;

            var randInt = ((int)(rnd.NextDouble() * range));

            return from + randInt;
        }

        public static T GetRandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(new Random().Next(v.Length));
        }
    }
}
