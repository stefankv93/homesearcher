﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeSearcher.Models
{
    public class XMLUserProvider : IUserProvider
    {
        public string FileName
        {
            get; set;
        }

        public List<AbstractUser> GetUsers()
        {
            if (string.IsNullOrEmpty(FileName))
            {
                throw new Exception("Empty or null file name for deserialization");
            }

            // Create sample file; replace if exists.
            Windows.Storage.StorageFolder storageFolder =
                 Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.ApplicationModel.Package.Current.InstalledLocation;
            var task = storageFolder.GetFileAsync(FileName).AsTask();
            task.Wait();
            Windows.Storage.StorageFile sampleFile = task.Result;
                    //await storageFolder.GetFileAsync(FileName).AsTask().Wait();

            List<AbstractUser> deserializedUsers = new List<AbstractUser>();

            using (var stream = sampleFile.OpenStreamForReadAsync().Result)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<AbstractUser>));
                deserializedUsers = (List<AbstractUser>)serializer.Deserialize(stream);
            }

            return deserializedUsers;
        }

        public async void SetUsers(List<AbstractUser> users)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                throw new Exception("Empty or null file name for deserialization");
            }

            // Create sample file; replace if exists.
            Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
            //Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile sampleFile =
                await storageFolder.CreateFileAsync(FileName,
                    Windows.Storage.CreationCollisionOption.ReplaceExisting);

            using (var stream = sampleFile.OpenStreamForWriteAsync().Result)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<AbstractUser>));
                serializer.Serialize(stream, users);
            }
        }
    }
}
