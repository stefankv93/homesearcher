﻿using HomeSearcher.Commands;
using HomeSearcher.ContentDialogs;
using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace HomeSearcher.ViewModels
{
    public class EstatesViewModel : ViewModelBase
    {
        public enum EstatesViewModelFilter { ForSale, ForRent, Like, None };
        private EstatesViewModelFilter _viewModelFilter = EstatesViewModelFilter.None;

        private Models.AbstractUser.Role _userRole;
        public Models.AbstractUser.Role UserRole
        {
            get { return _userRole; }
            set
            {
                _userRole = value;
                NotifyPropertyChanged(nameof(UserRole));
            }
        }

        ObservableCollection<RealEstate> _estates;
        public ObservableCollection<RealEstate> Estates
        {
            get { return _estates; }
            set
            {
                //if (_estates != value)
                {
                    _estates = value;
                    NotifyPropertyChanged(nameof(Estates));
                }
            }
        }

        public RelayCommand FilterView { get; private set; }

        public RelayCommand<RealEstate> DetailEstateCommand {get; private set; }

        public RelayCommand<RealEstate> NewCommentCommand { get; private set; }

        public RelayCommand<RealEstate> ShareCommand { get; private set; }

        public RelayCommand<RealEstate> LikeCommand { get; private set; }

        public RelayCommand<RealEstate> SeeLikesCommand { get; private set; }

        public RelayCommand<RealEstate> SeeCommentsCommand { get; private set; }

        public RelayCommand<RealEstate> MakeApointmentCommand { get; private set; }

        public RelayCommand AddEstateCommand { get; private set; }

        public RelayCommand<RealEstate> RemoveEstateCommand { get; private set; }

        public RelayCommand<RealEstate> EditEstateCommand { get; private set; }

        public EstatesViewModel(Models.AbstractUser.Role role, EstatesViewModelFilter estatesViewModelFilter = EstatesViewModelFilter.None)
        {
            _viewModelFilter = estatesViewModelFilter;
            _userRole = role;

            RefreshEstates();

            FilterView = new RelayCommand(async () =>
            {

                var content = new FilteringContentDialog(ApplicationEnviroment.Instance.RealEstatesContainer.GetRealEstates());
                await content.ShowAsync();

                if (estatesViewModelFilter == EstatesViewModelFilter.Like)
                {
                    Estates = new ObservableCollection<RealEstate>(content.EstateListResult.Where((estate) => estate.IsLikedByCurrentUser));
                }
                else if (estatesViewModelFilter == EstatesViewModelFilter.ForRent)
                {
                    Estates = new ObservableCollection<RealEstate>(content.EstateListResult.Where((estate) => estate.SaleType == RealEstate.EstateSaleType.ForRent));
                }
                else if (estatesViewModelFilter == EstatesViewModelFilter.ForSale)
                {
                    Estates = new ObservableCollection<RealEstate>(content.EstateListResult.Where((estate) => estate.SaleType == RealEstate.EstateSaleType.ForSale));
                }
                else
                {
                    Estates = new ObservableCollection<RealEstate>(content.EstateListResult);
                }
            });

            DetailEstateCommand = new RelayCommand<RealEstate>(DetailEstateCommandTap);

            NewCommentCommand = new RelayCommand<RealEstate>(NewCommentCommandTap);

            ShareCommand = new RelayCommand<RealEstate>(ShareCommandTap);

            LikeCommand = new RelayCommand<RealEstate>(LikeCommandTap);

            SeeLikesCommand = new RelayCommand<RealEstate>(SeeLikesCommandTap);

            SeeCommentsCommand = new RelayCommand<RealEstate>(SeeCommentsCommandTap);

            MakeApointmentCommand = new RelayCommand<RealEstate>(MakeApointmentCommandTap);

            AddEstateCommand = new RelayCommand(AddEstateCommandTap);

            RemoveEstateCommand = new RelayCommand<RealEstate>(async (estate) =>
            {
                ContentDialog removeEstateDialog = new ContentDialog
                {
                    Title = "Are you sure you want to remove this estate?",
                    Content = "If you delete this estate, you won't be able to recover it. Do you want to remove it?",
                    PrimaryButtonText = "Remove",
                    CloseButtonText = "Cancel"
                };

                removeEstateDialog.Background = Application.Current.Resources["LightBlueBrush"] as Brush;
                removeEstateDialog.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

                ContentDialogResult result = await removeEstateDialog.ShowAsync();

                // Delete the file if the user clicked the primary button.
                /// Otherwise, do nothing.
                if (result == ContentDialogResult.Primary)
                {
                    ApplicationEnviroment.Instance.RealEstatesContainer.RemoveEstate(estate.Id);
                    RefreshEstates();
                }
                else
                {
                    // The user clicked the CLoseButton, pressed ESC, Gamepad B, or the system back button.
                    // Do nothing.
                }
            });

            EditEstateCommand = new RelayCommand<RealEstate>(async (estate) =>
            {
                EstateContentDialog estateContentDialog = new EstateContentDialog(estate);

                await estateContentDialog.ShowAsync();

                if (estateContentDialog.PressedSave)
                {
                    RefreshEstates();
                }
            });
        }

        private void RefreshEstates()
        {
            _estates = new ObservableCollection<RealEstate>(ApplicationEnviroment.Instance.RealEstatesContainer.GetRealEstates());

            if (_viewModelFilter == EstatesViewModelFilter.Like)
            {
                _estates = new ObservableCollection<RealEstate>(_estates.Where((estate) => estate.IsLikedByCurrentUser));
            }
            else if (_viewModelFilter == EstatesViewModelFilter.ForRent)
            {
                _estates = new ObservableCollection<RealEstate>(_estates.Where((estate) => estate.SaleType == RealEstate.EstateSaleType.ForRent));
            }
            else if (_viewModelFilter == EstatesViewModelFilter.ForSale)
            {
                _estates = new ObservableCollection<RealEstate>(_estates.Where((estate) => estate.SaleType == RealEstate.EstateSaleType.ForSale));
            }

            Estates = _estates;
        }

        private async void AddEstateCommandTap()
        {
            RealEstate estate = new RealEstate()
            {
                Location = string.Empty,
                DateCreation = DateTime.Now,
                Area = 0,
                Rooms = 0,
                Floor =0,
                Price = 0,
                Type = RealEstate.EstateType.None,
                SaleType = RealEstate.EstateSaleType.None,
                HeatingType = RealEstate.Heating.None,
                HasTerrace = false,
                IsRegistered = false
            };

            EstateContentDialog dialog = new EstateContentDialog(estate);

            await dialog.ShowAsync();

            if (dialog.PressedSave)
            {
                ApplicationEnviroment.Instance.RealEstatesContainer.AddRealEstate(estate);
                RefreshEstates();
            }
        }

        private void MakeApointmentCommandTap(RealEstate obj)
        {
            int a = 0;
            return;
        }

        private void DetailEstateCommandTap(RealEstate estate)
        {
            ApplicationEnviroment.Instance.NavigationFacade.NavigateToEstateDetialsPage(estate);
        }

        private void LikeCommandTap(RealEstate estate)
        {
            if(estate.IsLikedByCurrentUser)
            {
                // Dislike the estate
                estate.RemoveUserLike(ApplicationEnviroment.Instance.CurrentUser.UserName);
            }
            else
            {
                // Like the page
                estate.AddUserLike(ApplicationEnviroment.Instance.CurrentUser.UserName);
            }
        }

        private void SeeLikesCommandTap(RealEstate estate)
        {
            int a = 0;
            return;
        }

        private async void NewCommentCommandTap(RealEstate estate)
        {
            var commentsDialg = new CommentsContentDialog(estate);
            await commentsDialg.ShowAsync();
        }

        private async void SeeCommentsCommandTap(RealEstate estate)
        {
            var commentsDialg = new CommentsContentDialog(estate);
            await commentsDialg.ShowAsync();
        }

        private async void ShareCommandTap(RealEstate estate)
        {
            ShareContentDialog dialog = new ShareContentDialog();

            await dialog.ShowAsync();
        }
    }
}
