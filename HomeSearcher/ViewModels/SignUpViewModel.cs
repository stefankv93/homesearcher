﻿using HomeSearcher.Commands;
using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace HomeSearcher.ViewModels
{
    public class SignUpViewModel : ViewModelBase
    {
        private string _username;
        public string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                NotifyPropertyChanged(nameof(UserName));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                NotifyPropertyChanged(nameof(Password));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _repeatPassword;
        public string RepeatPassword
        {
            get
            {
                return _repeatPassword;
            }
            set
            {
                _repeatPassword = value;
                NotifyPropertyChanged(nameof(RepeatPassword));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _emailAddress;
        public string EmailAddress
        {
            get
            {
                return _emailAddress;
            }
            set
            {
                _emailAddress = value;
                NotifyPropertyChanged(nameof(EmailAddress));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _firstName;
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                NotifyPropertyChanged(nameof(FirstName));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _secondName;
        public string SecondName
        {
            get
            {
                return _secondName;
            }
            set
            {
                _secondName = value;
                NotifyPropertyChanged(nameof(SecondName));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
                NotifyPropertyChanged(nameof(PhoneNumber));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _livingAddress;
        public string LivingAddress
        {
            get
            {
                return _livingAddress;
            }
            set
            {
                _livingAddress = value;
                NotifyPropertyChanged(nameof(LivingAddress));
                if (SignUpCommand != null)
                {
                    SignUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _imageLocation;
        public string ImageLocation
        {
            get
            {
                return _imageLocation;
            }
            set
            {
                //if(_imageLocation != value)
                {
                    _imageLocation = value;
                    NotifyPropertyChanged(nameof(ImageLocation));
                }
            }
        }

        public RelayCommand SignUpCommand { get; set; }

        public RelayCommand ClearAllCommand { get; set; }

        public RelayCommand ChooseImageCommand { get; set; }

        public SignUpViewModel()
        {
            UserName = string.Empty;
            Password = string.Empty;
            RepeatPassword = string.Empty;
            EmailAddress = string.Empty;
            FirstName = string.Empty;
            SecondName = string.Empty;
            PhoneNumber = string.Empty;
            LivingAddress = string.Empty;
            ImageLocation = @"ms-appx:///Assets/UserPhotos/default.png";

            SignUpCommand = new RelayCommand(async () => {
                if(!string.Equals(Password, RepeatPassword))
                {
                    var dialog = new MessageDialog(string.Format("User {0} already registered on a system, please provide other user name", UserName),
                    "PasswordError");

                    await dialog.ShowAsync();
                    return;
                }

                AbstractUser user = new User(UserName, Password, FirstName, SecondName, EmailAddress, PhoneNumber, LivingAddress);
                if (ApplicationEnviroment.Instance.UserContainer.AddUser(user))
                {
                    var dialog = new MessageDialog(string.Format("User {0} already registered on a system, please provide other user name", UserName),
                    "Username error");

                    await dialog.ShowAsync();
                    return;
                }
                else
                {
                    var dialog = new MessageDialog(string.Format("User {0} successfully registered on a system", UserName),
                    "Success");

                    await dialog.ShowAsync();

                    var sourceFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(new Uri(this.ImageLocation));
                    await sourceFile.RenameAsync(UserName + ".jpg");

                    // navigate to user page
                    ApplicationEnviroment.Instance.NavigationFacade.NavigateToUserView();
                    return;
                }

            },
            () =>
            {
                return UserName.Length > 0 && Password.Length > 0 && RepeatPassword.Length > 0
                    && EmailAddress.Length > 0 && FirstName.Length > 0 && SecondName.Length > 0
                    && PhoneNumber.Length > 0 && LivingAddress.Length > 0;
            });

            ClearAllCommand = new RelayCommand(() =>
            {
                UserName = string.Empty;
                Password = string.Empty;
                RepeatPassword = string.Empty;
                EmailAddress = string.Empty;
                FirstName = string.Empty;
                SecondName = string.Empty;
                PhoneNumber = string.Empty;
                LivingAddress = string.Empty;
                ImageLocation = @"ms-appx:///Assets/UserPhotos/default.png";
            });

            ChooseImageCommand = new RelayCommand(async () =>
            {
                var picker = new Windows.Storage.Pickers.FileOpenPicker();
                picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
                picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
                picker.FileTypeFilter.Add(".jpg");

                Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();

                if (file != null)
                {
                    // Application now has read/write access to the picked file
                    Windows.Storage.StorageFolder storageFolder =
                        Windows.Storage.ApplicationData.Current.LocalFolder;
                    Windows.Storage.StorageFolder destinationFolder = (Windows.Storage.StorageFolder)await storageFolder.TryGetItemAsync(Constants.Constants.LocalUserPhotosFolderName);

                    Windows.Storage.StorageFile destinationFile = (Windows.Storage.StorageFile)await destinationFolder.TryGetItemAsync(Constants.Constants.LocalUserPhotosCurrentImage);
                    if(destinationFile != null)
                    {
                       await destinationFile.DeleteAsync();
                    }

                    await file.CopyAsync(destinationFolder);

                    destinationFile = await destinationFolder.GetFileAsync(file.Name);
                    await destinationFile.RenameAsync(Constants.Constants.LocalUserPhotosCurrentImage);

                    this.ImageLocation = @"ms-appdata:///local/UserPhotos/currentImage.jpg"; //destinationFile.Path;
                }
            });
        }

    }
}
