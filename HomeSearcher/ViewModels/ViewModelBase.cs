﻿using HomeSearcher.ComponentModel;
using System.Threading.Tasks;

namespace HomeSearcher.ViewModels
{
    public abstract class ViewModelBase : ObservableObjectBase
    {
        /// <summary>
        /// Loads the state.
        /// </summary>
        public virtual Task LoadState()
        {
            return Task.CompletedTask;
        }
    }
}
