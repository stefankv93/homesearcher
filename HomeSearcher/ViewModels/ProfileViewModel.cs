﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.ViewModels
{
    public class ProfileViewModel : ViewModelBase
    {
        private AbstractUser _user;
        public AbstractUser User
        {
            get
            {
                return _user;
            }
            set
            {
                if(_user != value)
                {
                    _user = value;
                    NotifyPropertyChanged(nameof(User));
                }
            }
        }

        public ProfileViewModel()
        {
            this.User = ApplicationEnviroment.Instance.CurrentUser;
        }
    }
}
