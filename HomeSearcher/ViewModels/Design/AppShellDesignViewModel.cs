﻿using HomeSearcher.Navigation;
using System.Collections.Generic;

namespace HomeSearcher.ViewModels.Design
{
    public class AppShellDesignViewModel
    {
        public AppShellDesignViewModel()
        {
            NavigationBarMenuItems = new List<INavigationBarMenuItem>();

            BottomNavigationBarMenuItems = new List<INavigationBarMenuItem>
            {
                new SettingsNavigationBarMenuItem(),
                //new DebugNavigationBarMenuItem()
            };
        }

        public List<INavigationBarMenuItem> BottomNavigationBarMenuItems { get; }

        public List<INavigationBarMenuItem> NavigationBarMenuItems { get; private set; }
    }
}
