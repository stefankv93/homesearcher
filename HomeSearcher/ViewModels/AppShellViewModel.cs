﻿using HomeSearcher.Models;
using HomeSearcher.Navigation;
using HomeSearcher.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HomeSearcher.ViewModels
{
    public class AppShellViewModel : ViewModelBase
    {
        AppShell _shell;
        public AppShellViewModel(AppShell shell)
        {
            _shell = shell;
            NavigationBarMenuItems = new ObservableCollection<INavigationBarMenuItem>();
            BottomNavigationBarMenuItems = new ObservableCollection<INavigationBarMenuItem>();
            ApplicationEnviroment.Instance.CurrentUserChanged += Instance_CurrentUserChanged;
            Instance_CurrentUserChanged(null, ApplicationEnviroment.Instance.CurrentUser);
        }

        private void Instance_CurrentUserChanged(object sender, AbstractUser e)
        {
            // Add login elements
            NavigationBarMenuItems.Clear();
            BottomNavigationBarMenuItems.Clear();

            if (e == null)
            {
                NavigationBarMenuItems.Add(new SignInNavigationBarMenuItem());
                NavigationBarMenuItems.Add(new SignUpNavigationBarMenuItem());
                NavigationBarMenuItems.Add(new ForgotPasswordNavigationBarMenuItem());

                BottomNavigationBarMenuItems.Add(new SettingsNavigationBarMenuItem());
            }
            else if(e.UserRole == AbstractUser.Role.User)
            {
                NavigationBarMenuItems.Add(new ForRentNavigationBarMenuItem());
                NavigationBarMenuItems.Add(new ForSaleNavigationBarMenuItem());
                NavigationBarMenuItems.Add(new LikedNavigationBarMenuItem());
                NavigationBarMenuItems.Add(new ApointmentNavigationBarMenutItem());

                BottomNavigationBarMenuItems.Add(new ProfileNavigationBarMenuItem());
                BottomNavigationBarMenuItems.Add(new SettingsNavigationBarMenuItem());
                BottomNavigationBarMenuItems.Add(new SignOutNavigationBarMenuItem());

                _shell.UpdateSelectionState();
            }
            else if(e.UserRole == AbstractUser.Role.Admin)
            {
                NavigationBarMenuItems.Add(new EstatesNavigationBarMenuItem());

                BottomNavigationBarMenuItems.Add(new ProfileNavigationBarMenuItem());
                BottomNavigationBarMenuItems.Add(new SettingsNavigationBarMenuItem());
                BottomNavigationBarMenuItems.Add(new SignOutNavigationBarMenuItem());

                _shell.UpdateSelectionState();
            }

            
        }

        /// <summary>
        /// The navigation bar items at the bottom.
        /// </summary>
        public ObservableCollection<INavigationBarMenuItem> BottomNavigationBarMenuItems { get; private set; }

        /// <summary>
        /// The navigation bar items at the top.
        /// </summary>
        public ObservableCollection<INavigationBarMenuItem> NavigationBarMenuItems { get; private set; }
    }
}
