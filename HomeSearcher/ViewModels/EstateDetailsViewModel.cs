﻿using HomeSearcher.Commands;
using HomeSearcher.ContentDialogs;
using HomeSearcher.Models;
using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace HomeSearcher.ViewModels
{
    public class EstateDetailsViewModel : ViewModelBase
    {
        private RealEstate _realEstate;
        public RealEstate RealEstate
        {
            get { return _realEstate; }
            set
            {
                //if(_realEstate != value)
                {
                    _realEstate = value;
                    NotifyPropertyChanged(nameof(RealEstate));
                }
            }
        }

        private AbstractUser.Role _userRole;
        public AbstractUser.Role UserRole
        {
            get { return _userRole; }
            set
            {
                if(_userRole != value)
                {
                    _userRole = value;
                    NotifyPropertyChanged(nameof(UserRole));
                }
            }
        }

        public RelayCommand LikeCommand { get; private set; }
        public RelayCommand MakeApointmentCommand { get; private set; }
        public RelayCommand RemoveEstateCommand { get; private set; }
        public RelayCommand EditEstateCommand { get; private set; }
        public RelayCommand ShareCommand { get; private set; }

        public EstateDetailsViewModel(RealEstate _realEstate)
        {
            this._realEstate = _realEstate;
            this._userRole = ApplicationEnviroment.Instance.CurrentUser.UserRole;

            LikeCommand = new RelayCommand(SeeLikesCommandTap);

            MakeApointmentCommand = new RelayCommand(MakeApointmentCommandTap);

            RemoveEstateCommand = new RelayCommand(RemoveEstateCommandTap);

            EditEstateCommand = new RelayCommand(EditEstateCommandTap);

            ShareCommand = new RelayCommand(async ()=>
            {
                ShareContentDialog dialog = new ShareContentDialog();

                await dialog.ShowAsync();
            });
        }

        private async void EditEstateCommandTap()
        {
            EstateContentDialog estateContentDialog = new EstateContentDialog(_realEstate);

            await estateContentDialog.ShowAsync();

            if(estateContentDialog.PressedSave)
            {
                RealEstate = estateContentDialog.RealEstate;
            }
        }

        private async void RemoveEstateCommandTap()
        {
            ContentDialog removeEstateDialog = new ContentDialog
            {
                Title = "Are you sure you want to remove this estate?",
                Content = "If you delete this estate, you won't be able to recover it. Do you want to remove it?",
                PrimaryButtonText = "Remove",
                CloseButtonText = "Cancel"
            };

            removeEstateDialog.Background = Application.Current.Resources["LightBlueBrush"] as Brush;
            removeEstateDialog.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

            ContentDialogResult result = await removeEstateDialog.ShowAsync();

            // Delete the file if the user clicked the primary button.
            /// Otherwise, do nothing.
            if (result == ContentDialogResult.Primary)
            {
                ApplicationEnviroment.Instance.RealEstatesContainer.RemoveEstate(_realEstate.Id);
                ApplicationEnviroment.Instance.NavigationFacade.GoBack();
            }
            else
            {
                // The user clicked the CLoseButton, pressed ESC, Gamepad B, or the system back button.
                // Do nothing.
            }
        }

        private async void MakeApointmentCommandTap()
        {
            AppointmentContentDialog dialog = new AppointmentContentDialog();

            await dialog.ShowAsync();

            if(dialog.time.HasValue)
            {
                ((User)ApplicationEnviroment.Instance.CurrentUser).Apointments.Add(new Apointment() { AppointmentDate = dialog.time.Value, RealEstate = _realEstate });
            }
        }

        private void SeeLikesCommandTap()
        {
            if (_realEstate.IsLikedByCurrentUser)
            {
                // Dislike the estate
                _realEstate.RemoveUserLike(ApplicationEnviroment.Instance.CurrentUser.UserName);
            }
            else
            {
                // Like the page
                _realEstate.AddUserLike(ApplicationEnviroment.Instance.CurrentUser.UserName);
            }
        }
    }
}
