﻿using HomeSearcher.Commands;
using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace HomeSearcher.ViewModels
{
    public class SignInViewModel : ViewModelBase
    {
        private string _username;
        public string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                NotifyPropertyChanged(nameof(UserName));
                if(SignInCommand != null)
                {
                    SignInCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                NotifyPropertyChanged(nameof(Password));
                if (SignInCommand != null)
                {
                    SignInCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public RelayCommand SignInCommand { get; private set; }

        public SignInViewModel()
        {
            UserName = string.Empty;
            Password = string.Empty;

            SignInCommand = new RelayCommand(async () =>
            {
                AbstractUser user = ApplicationEnviroment.Instance.UserContainer.GetUser(UserName);
                if (user == null)
                {
                    var dialog = new MessageDialog(string.Format("User {0} doesn't exist", UserName),
                    "Username error");

                    await dialog.ShowAsync();
                    return;
                }

                if(!user.Password.Equals(Password))
                {
                    var dialog = new MessageDialog(string.Format("Password for user {0} isn't correct", UserName),
                    "Password error");

                    await dialog.ShowAsync();
                    return;
                }

                ApplicationEnviroment.Instance.CurrentUser = user;

                if(user.UserRole == AbstractUser.Role.Admin)
                {
                    // navigate to admin page
                    ApplicationEnviroment.Instance.NavigationFacade.NavigateToAdminView();
                    ApplicationEnviroment.Instance.NavigationFacade.RemoveBackStackFrames(1);
                }
                else if(user.UserRole == AbstractUser.Role.User)
                {
                    // navigate to user page
                    ApplicationEnviroment.Instance.NavigationFacade.NavigateToUserView();
                    ApplicationEnviroment.Instance.NavigationFacade.RemoveBackStackFrames(1);
                }
                else
                {
                    var dialog = new MessageDialog("Agent view isn't supported in this version of app",
                    "Agent error");

                    await dialog.ShowAsync();
                    return;
                }
            },
            () =>
            {
                return UserName.Length > 0 && Password.Length > 0;
            });
        }
    }
}
