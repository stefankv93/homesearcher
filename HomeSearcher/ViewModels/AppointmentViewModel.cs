﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.ViewModels
{
    public class AppointmentViewModel : ViewModelBase
    {
        List<Apointment> _appointments;
        public List<Apointment> Appointments
        {
            get { return _appointments; }
            set
            {
                _appointments = value;
                NotifyPropertyChanged(nameof(Appointments));
            }
        }
        public AppointmentViewModel()
        {
            _appointments = new List<Apointment>();

            if(((User)ApplicationEnviroment.Instance.CurrentUser).Apointments != null)
            {
                _appointments = ((User)ApplicationEnviroment.Instance.CurrentUser).Apointments;
            }
        }
    }
}
