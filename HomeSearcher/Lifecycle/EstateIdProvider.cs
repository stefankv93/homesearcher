﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace HomeSearcher.Lifecycle
{
    /// <summary>
    /// Manages real estate ids
    /// </summary>
    public class EstateIdProvider
    {
        private const string IdKey = "ID_KEY";

        /// <summary>
        /// Determines whether the app has been launched for the first time.
        /// </summary>
        /// <returns>True, if launched for first time. Otherwise, false.</returns>
        private static bool IsFirstLaunch()
        {
            var value = ApplicationData.Current.LocalSettings.Values[IdKey];

            return (value == null || (int)value < 1);
        }

        /// <summary>
        /// Returns estate id
        /// </summary>
        public static int GetRealEstateId()
        {
            if(IsFirstLaunch())
            {
                ApplicationData.Current.LocalSettings.Values[IdKey] = 0;
            }

            var value = (int)ApplicationData.Current.LocalSettings.Values[IdKey];

            ApplicationData.Current.LocalSettings.Values[IdKey] = value + 1;

            return value;
        }
    }
}
