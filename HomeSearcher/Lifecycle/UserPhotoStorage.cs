﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace HomeSearcher.Lifecycle
{
    class UserPhotoStorage
    {
        private const string UserPhotosKey = "_USER_PHOTOS_KEY";

        /// <summary>
        /// Determines whether the app has been launched for the first time.
        /// </summary>
        /// <returns>True, if launched for first time. Otherwise, false.</returns>
        private static bool IsFirstLaunch()
        {
            var value = ApplicationData.Current.LocalSettings.Values[UserPhotosKey];

            return (value == null || (int)value < 1);
        }

        public static void SaveUserPhoto(Windows.Storage.StorageFile photo, string userName)
        {

        }
    }
}
