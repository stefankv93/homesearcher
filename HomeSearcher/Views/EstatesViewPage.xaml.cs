﻿using HomeSearcher.Models;
using HomeSearcher.ViewModels;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml;
using System;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EstatesViewPage : BasePage
    {
        private EstatesViewModel _viewModel;
        //private const int ItemMargin = 6;
        private double PreferredImageWidth = 300;
        private Thickness _imageMargin;
        private double _imageHeight;
        private double _imageWidth;

        /// <summary>
        /// Gets or sets the margin of images in the stream.
        /// </summary>
        public Thickness ImageMargin
        {
            get { return _imageMargin; }
            set
            {
                if (_imageMargin != value)
                {
                    _imageMargin = value;
                    NotifyPropertyChanged(nameof(ImageMargin));
                }
            }
        }

        /// <summary>
        /// Gets or sets the margin of images in the stream.
        /// </summary>
        public double ImageHeight
        {
            get { return _imageHeight; }
            set
            {
                if (_imageHeight != value)
                {
                    _imageHeight = value;
                    NotifyPropertyChanged(nameof(ImageHeight));
                }
            }
        }

        /// <summary>
        /// Gets or sets the margin of images in the stream.
        /// </summary>
        public double ImageWidth
        {
            get { return _imageWidth; }
            set
            {
                if (_imageWidth != value)
                {
                    _imageWidth = value;
                    NotifyPropertyChanged(nameof(ImageWidth));
                }
            }
        }

        public EstatesViewPage()
        {
            this.InitializeComponent();

            UpdateImageWidth();
            UpdateImageHeight();

            SizeChanged += EstateViewPage_SizeChanged;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            EstatesViewModel.EstatesViewModelFilter filter = EstatesViewModel.EstatesViewModelFilter.None;
            if(e.Parameter != null)
            {
                filter = (EstatesViewModel.EstatesViewModelFilter)Enum.Parse(typeof(EstatesViewModel.EstatesViewModelFilter), (string)e.Parameter);
            }
            _viewModel = new EstatesViewModel(ApplicationEnviroment.Instance.CurrentUser.UserRole, filter);
            DataContext = _viewModel;
        }

        private void EstateViewPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateImageWidth();
            UpdateImageHeight();
        }

        private void UpdateImageHeight()
        {
            // Calculating the additional height needed per item
            // depending on the overall page width.
            int additionalHeightPerItem;

            if (pageRoot.ActualWidth < 720)
            {
                additionalHeightPerItem = (int)(ImageWidth / 5);
            }
            else if (pageRoot.ActualWidth > 1900)
            {
                additionalHeightPerItem = (int)(ImageWidth / 10);
            }
            else
            {
                additionalHeightPerItem = (int)(ImageWidth / 7.5);
            }

            ImageHeight = ImageWidth + additionalHeightPerItem;
        }

        private void UpdateImageWidth()
        {
            if (pageRoot.ActualWidth < 720)
            {
                ImageWidth = pageRoot.ActualWidth;
                ImageMargin = new Thickness(0, 0, 0, 6);
            }
            else
            {
                // Calculating how many items per row we get with preferred with + image margin
                var itemsPerRow = (int)(pageRoot.ActualWidth / (PreferredImageWidth /*+ ItemMargin */+ 10));

                // Calculating rest and distribute among items
                var rest = (int)(pageRoot.ActualWidth % (PreferredImageWidth/*+ ItemMargin*/ + 10));
                var additionalWidthPerItem = rest / itemsPerRow;

                ImageWidth = PreferredImageWidth + additionalWidthPerItem - 6;
                //ImageMargin = new Thickness(0, 0, ItemMargin, ItemMargin);
            }
        }
    }
}
