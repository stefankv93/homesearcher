﻿using HomeSearcher.Controls;
using HomeSearcher.Models;
using HomeSearcher.Navigation;
using HomeSearcher.ViewModels;
using System;
using System.Collections.Specialized;
using System.Linq;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Automation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AppShell : Page
    {
        public static AppShell Current;
        private AppShellViewModel _viewModel;
        private Tuple<Type, object> _lastSourcePageEventArgs;

        public AppShell()
        {
            this.InitializeComponent();

            // Set the data context
            _viewModel = new AppShellViewModel(this);
            DataContext = _viewModel;

            Loaded += (sender, args) =>
            {
                Current = this;

                togglePaneButton.Focus(FocusState.Programmatic);
                // We need to update the initial selection because
                // OnNavigatingToPage happens before Items are loaded in
                // both navigation bars.
                UpdateSelectionState();
            };

            rootSplitView.RegisterPropertyChangedCallback(
               SplitView.DisplayModeProperty,
               (s, a) =>
               {
                    // Ensure that we update the reported size of the TogglePaneButton when the SplitView's
                    // DisplayMode changes.
                    CheckTogglePaneButtonSizeChanged();
               });

            SystemNavigationManager.GetForCurrentView().BackRequested += SystemNavigationManager_BackRequested;

            var titleBar = ApplicationView.GetForCurrentView().TitleBar;
            titleBar.ButtonBackgroundColor = Application.Current.Resources["DarkBlue"] as Color?;//Colors.Blue;
        }

        private void AppShell_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateSelectionState();
        }

        /// <summary>
        /// Gets the app frame.
        /// </summary>
        public Frame AppFrame
        {
            get { return frame; }
        }

        /// <summary>
        /// The rectangle of the toggle button.
        /// </summary>
        public Rect TogglePaneButtonRect { get; private set; }

        /// <summary>
        /// An event to notify listeners when the hamburger button may occlude other content in the app.
        /// The custom "PageHeader" user control is using this.
        /// </summary>
        public event TypedEventHandler<AppShell, Rect> TogglePaneButtonRectChanged;

        private void OnNavigatedToPage(object sender, NavigationEventArgs e)
        {
            // Update the Back button depending on whether we can go Back.
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                AppFrame.CanGoBack ?
                AppViewBackButtonVisibility.Visible :
                AppViewBackButtonVisibility.Collapsed;

            _lastSourcePageEventArgs = new Tuple<Type, object>(e.SourcePageType, e.Parameter);

            UpdateSelectionState();
        }

        private void SystemNavigationManager_BackRequested(object sender, BackRequestedEventArgs e)
        {
            if (!e.Handled && AppFrame.CanGoBack)
            {
                e.Handled = true;
                AppFrame.GoBack();
            }
        }

        /// <summary>
        /// Enable accessibility on each nav menu item by setting the AutomationProperties.Name on each container
        /// using the associated Label of each item.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The event arguments.</param>
        private void NavMenuItemContainerContentChanging(ListViewBase sender, ContainerContentChangingEventArgs args)
        {
            if (!args.InRecycleQueue && args.Item is INavigationBarMenuItem)
            {
                args.ItemContainer.SetValue(AutomationProperties.NameProperty,
                    ((INavigationBarMenuItem)args.Item).Label);
            }
            else
            {
                args.ItemContainer.ClearValue(AutomationProperties.NameProperty);
            }
        }

        /// <summary>
        /// Navigate to the Page for the selected <paramref name="listViewItem" />.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="listViewItem">The ListViewItem.</param>
        private void NavMenuList_ItemInvoked(object sender, ListViewItem listViewItem)
        {
            var item = (INavigationBarMenuItem)((NavMenuListView)sender).ItemFromContainer(listViewItem);

            if (item != null)
            {
                // We navigate only if current page is different to target page
                // or if navigation arguments are available.
                if ((item.DestPage != null
                    /*&& item.DestPage != AppFrame.CurrentSourcePageType*/)
                    || _lastSourcePageEventArgs.Item2 != null)
                {
                    if(item is SignOutNavigationBarMenuItem)
                    {
                        ApplicationEnviroment.Instance.CurrentUser = null;
                    }
                    AppFrame.Navigate(item.DestPage, item.Arguments);
                }
            }
        }

        /// <summary>
        /// Callback when the SplitView's Pane is toggled open or close.  When the Pane is not visible
        /// then the floating hamburger may be occluding other content in the app unless it is aware.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        private void TogglePaneButton_Checked(object sender, RoutedEventArgs e)
        {
            CheckTogglePaneButtonSizeChanged();
        }

        /// <summary>
        /// Check for the conditions where the navigation pane does not occupy the space under the floating
        /// hamburger button and trigger the event.
        /// </summary>
        private void CheckTogglePaneButtonSizeChanged()
        {
            if (rootSplitView.DisplayMode == SplitViewDisplayMode.Inline ||
                rootSplitView.DisplayMode == SplitViewDisplayMode.Overlay)
            {
                var transform = togglePaneButton.TransformToVisual(this);
                var rect =
                    transform.TransformBounds(new Rect(0, 0, togglePaneButton.ActualWidth,
                        togglePaneButton.ActualHeight));
                TogglePaneButtonRect = rect;
            }
            else
            {
                TogglePaneButtonRect = new Rect();
            }

            TogglePaneButtonRectChanged?.DynamicInvoke(this, TogglePaneButtonRect);
        }

        public void UpdateNavMenuList()
        {
            _viewModel = new AppShellViewModel(this);
            DataContext = _viewModel;
        }

        public void UpdateSelectionState()
        {
            
            //navMenuList.SetSelectedItem(null);
            //bottomNavMenuList.SetSelectedItem(null);

            INavigationBarMenuItem itemToHighlight = (INavigationBarMenuItem)navMenuList.SelectedItem;
            if(itemToHighlight == null)
            {
                itemToHighlight = (INavigationBarMenuItem)bottomNavMenuList.SelectedItem;
            }
            //ViewModelArgs viewModelArgs = null;

            var parameter = _lastSourcePageEventArgs.Item2 as string;

            /*if (parameter != null)
            {
                viewModelArgs = SerializationHelper.Deserialize<ViewModelArgs>(parameter);
            }*/

            // We only highlight the current page in the navigation bar
            // if navigation arguments tell us to.
            //if (viewModelArgs == null
              //  || viewModelArgs.HighlightOnNavigationBar)
            if(_lastSourcePageEventArgs.Item1 == typeof(EstatesViewPage))
            {
                if(_lastSourcePageEventArgs.Item2 != null)
                {
                    //if(_lastSourcePageEventArgs.Item2 is User)
                    //{
                    if(_lastSourcePageEventArgs.Item2.Equals("ForRent"))
                    {
                        itemToHighlight = _viewModel.NavigationBarMenuItems.Where((menuItem) => menuItem.Label.Equals("For Rent")).SingleOrDefault();
                    }
                    else if(_lastSourcePageEventArgs.Item2.Equals("ForSale"))
                    {
                        itemToHighlight = _viewModel.NavigationBarMenuItems.Where((menuItem) => menuItem.Label.Equals("For Sale")).SingleOrDefault();
                    }
                    else if(_lastSourcePageEventArgs.Item2.Equals("Like"))
                    {
                        itemToHighlight = _viewModel.NavigationBarMenuItems.Where((menuItem) => menuItem.Label.Equals("Liked")).SingleOrDefault();
                    }
                    else if(_lastSourcePageEventArgs.Item2.Equals("None"))
                    {
                        itemToHighlight = _viewModel.NavigationBarMenuItems.Where((menuItem) => menuItem.Label.Equals("Estates")).SingleOrDefault();
                    }
                }
            }
            else
            {
                itemToHighlight =
                (from p in _viewModel.NavigationBarMenuItems.Union(_viewModel.BottomNavigationBarMenuItems)
                 where p.DestPage == _lastSourcePageEventArgs.Item1
                 select p)
                    .SingleOrDefault();
            }

            /*togglePaneButton.Background = itemToHighlight == null ?
                new SolidColorBrush(Colors.Red) : //new SolidColorBrush((Color)Application.Current.Resources["AppAccentLightColor"]) :
                new SolidColorBrush(Colors.White);*/

            navMenuList.UpdateLayout();
            bottomNavMenuList.UpdateLayout();

            ListViewItem container;

            if (navMenuList.Items != null && navMenuList.Items.Contains(itemToHighlight))
            {
                container = (ListViewItem)navMenuList.ContainerFromItem(itemToHighlight);
            }
            else
            {
                container = (ListViewItem)bottomNavMenuList.ContainerFromItem(itemToHighlight);
            }

            // While updating the selection state of the item prevent it from taking keyboard focus. If a
            // user is invoking the back button via the keyboard causing the selected nav menu item to change
            // then focus will remain on the back button.
            if (container != null)
            {
                container.IsTabStop = false;
                navMenuList.SetSelectedItem(null);
                bottomNavMenuList.SetSelectedItem(null);
                navMenuList.SetSelectedItem(container);
                bottomNavMenuList.SetSelectedItem(container);
            }

            if (container != null)
            {
                container.IsTabStop = true;
            }
        }
    }
}
