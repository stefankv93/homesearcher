﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace HomeSearcher.ValueConverters
{
    public class UserToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var targetRole = (parameter != null) ? (AbstractUser.Role)Enum.Parse(typeof(AbstractUser.Role), (string)parameter) : AbstractUser.Role.Admin;

            var currentRole = (AbstractUser.Role)value;

            if (targetRole == currentRole) return Visibility.Visible;

            else return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
