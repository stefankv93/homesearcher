﻿using System;
using Windows.UI.Xaml.Data;
using HomeSearcher.Extensions;

namespace HomeSearcher.ValueConverters
{
    /// <summary>
    /// Does conversion between <see cref="DateTime" /> and a human
    /// readable string which specifies the time passed.
    /// </summary>
    public class RelativeTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var dateTime = (DateTime)value;
            return dateTime.ToRelativeTime();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
