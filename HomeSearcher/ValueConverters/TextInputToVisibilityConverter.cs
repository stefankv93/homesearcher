﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace HomeSearcher.ValueConverters
{
    public class TextInputToVisibilityConverter : IValueConverter
    {
        public object Convert(object values, Type targetType, object parameter, string language)
        {
            var bools = values as List<bool>;
            // Always test MultiValueConverter inputs for non-null
            // (to avoid crash bugs for views in the designer)
            bool hasText = !(bool)bools[0];
            bool hasFocus = (bool)bools[1];

            if (hasFocus || hasText)
                return Visibility.Collapsed;

            return Visibility.Visible;
        }


        public object ConvertBack(object value, Type targetTypes, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
