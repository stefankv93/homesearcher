﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace HomeSearcher.ValueConverters
{
    public class EnumToStringConversion : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if(value == null) return "";

            if(value is RealEstate.EstateType)
            {
                switch((RealEstate.EstateType)value)
                {
                    case RealEstate.EstateType.None: return "None";
                    case RealEstate.EstateType.Apartment: return "Apartment";
                    case RealEstate.EstateType.ApartmentInHouse: return "Apartment in a house";
                    case RealEstate.EstateType.House: return "House";
                    case RealEstate.EstateType.OfficeSpace: return "Office space";
                    case RealEstate.EstateType.Shop: return "Shop";
                    
                }
            }
            else if(value is RealEstate.EstateSaleType)
            {
                switch((RealEstate.EstateSaleType)value)
                {
                    case RealEstate.EstateSaleType.None: return "None";
                    case RealEstate.EstateSaleType.ForRent: return "For Rent";
                    case RealEstate.EstateSaleType.ForSale: return "For Sale";
                }
            }
            else if(value is RealEstate.Heating)
            {
                switch ((RealEstate.Heating)value)
                {
                    case RealEstate.Heating.None: return "None";
                    case RealEstate.Heating.Central: return "Central";
                    case RealEstate.Heating.ElectricPower: return "Electric Power";
                    case RealEstate.Heating.Floor: return "Floor";
                    case RealEstate.Heating.StorageHeater: return "Storage Heater";
                    case RealEstate.Heating.Underfloor: return "Underfloor";
                }
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
