﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace HomeSearcher.ValueConverters
{
    class CommentsToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var comments = (List<Comment>)value;

            if(comments == null || comments.Count == 0)
            {
                return "Be the first to comment";
            }

            return string.Format("View all {0}", comments.Count);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
