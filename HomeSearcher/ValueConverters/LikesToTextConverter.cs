﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace HomeSearcher.ValueConverters
{
    class LikesToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var likes = (List<string>)value;
            if(likes == null || likes.Count == 0)
            {
                return "Be the first to like this estate";
            }

            if(likes.Count == 1 && likes[0].Equals(ApplicationEnviroment.Instance.CurrentUser.UserName))
            {
                return "Liked by you";
            }

            if(likes.Any((user) => user.Equals(ApplicationEnviroment.Instance.CurrentUser.UserName)))
            {
                return string.Format("Liked by you and {0} people more", likes.Count - 1);
            }

            return string.Format("Liked by {0} people", likes.Count);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
