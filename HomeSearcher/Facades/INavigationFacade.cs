﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Facades
{
    /// <summary>
    /// Encapsulates page navigation.
    /// </summary>
    public interface INavigationFacade
    {
        /// <summary>
        /// Goes back to the previews view(s).
        /// </summary>
        /// <param name="steps">Number of views to go back.</param>
        void GoBack(int steps = 1);

        /// <summary>
        /// Removes the specified number of frames from the back stack.
        /// </summary>
        /// <param name="numberOfFrames">The number of frames.</param>
        void RemoveBackStackFrames(int numberOfFrames);

        /// <summary>
        /// Navigates to the signed-in user's profile view.
        /// </summary>
        void NavigateToUserView();

        /// <summary>
        /// Navigates to the signed-in user's profile view.
        /// </summary>
        void NavigateToAdminView();

        /// <summary>
        /// Navigates to the real estate details page.
        /// </summary>
        void NavigateToEstateDetialsPage(RealEstate realEstate);
    }
}
