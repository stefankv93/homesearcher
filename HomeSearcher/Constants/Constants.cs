﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSearcher.Constants
{
    public class Constants
    {
        public static string LocalStorageFolderName = "Files";
        public static string LocalStorageUserFile = "users.xml";
        public static string LocalUserPhotosFolderName = "UserPhotos";
        public static string LocalUserPhotosCurrentImage = "currentImage.jpg";
        public static string LocalUserPhotoPathTemplate = @"ms-appdata:///local/UserPhotos/{0}.jpg";
    }
}
