﻿using System;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.ContentDialogs
{
    public sealed partial class ShareContentDialog : ContentDialog
    {
        public ShareContentDialog()
        {
            this.InitializeComponent();
            Share.Click += Share_Click;
            Cancel.Click += Cancel_Click;
        }

        private async void Share_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new MessageDialog("Succesfully shared real estate!");
            await dialog.ShowAsync();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
