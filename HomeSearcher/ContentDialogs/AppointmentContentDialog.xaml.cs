﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.ContentDialogs
{
    public sealed partial class AppointmentContentDialog : ContentDialog
    {
        public DateTime? time = null;

        public AppointmentContentDialog()
        {
            this.InitializeComponent();

            Submit.Click += Submit_Click;
            Cancel.Click += Cancel_Click;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void Submit_Click(object sender, RoutedEventArgs e)
        {
            if(AppointmentDate.Date.HasValue)
            {
                time = AppointmentDate.Date.Value.Date;
            }
            this.Hide();
        }


    }
}
