﻿using HomeSearcher.Models;
using HomeSearcher.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.ContentDialogs
{
    public sealed partial class EstateContentDialog : ContentDialog
    {
        private RealEstate _realEstate;
        public RealEstate RealEstate
        {
            get { return _realEstate; }
        }

        public bool PressedSave = false;
        public EstateContentDialog(
            RealEstate realEstate
            )
        {
            this.InitializeComponent();

            _realEstate = realEstate;

            if (realEstate.SaleType == RealEstate.EstateSaleType.ForSale)
            {
                ForSaleRadioButton.IsChecked = true;
            }
            else if(realEstate.SaleType == RealEstate.EstateSaleType.ForRent)
            {
                ForRentRadioButton.IsChecked = true;
            }

            LocationFromTextBox.Text = realEstate.Location;
            EstateTypeComboBox.SelectedIndex = (int)realEstate.Type;
            PriceFromTextBox.Text = realEstate.Price.ToString();
            AreaFromTextBox.Text = realEstate.Area.ToString();
            RoomsFromTextBox.Text = realEstate.Rooms.ToString();
            FloorFromTextBox.Text = realEstate.Floor.ToString();
            HeatingTypeComboBox.SelectedIndex = (int)realEstate.HeatingType;
            TerraceCheckBox.IsChecked = realEstate.HasTerrace;
            RegisteredCheckBox.IsChecked = realEstate.IsRegistered;

            SaveButton.Click += SaveButton_Click;
            CancelButton.Click += CancelButton_Click;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            PressedSave = true;
            if (ForSaleRadioButton.IsChecked.Value)
            {
                _realEstate.SaleType  = RealEstate.EstateSaleType.ForSale;
            }
            else if (ForRentRadioButton.IsChecked.Value)
            {
                _realEstate.SaleType = RealEstate.EstateSaleType.ForRent;
            }

            _realEstate.Location = LocationFromTextBox.Text;
            _realEstate.Type = EnumHelper.ConvertToEstateType((string)((ComboBoxItem)EstateTypeComboBox.SelectedItem).Content);
            _realEstate.Price = double.Parse(PriceFromTextBox.Text);
            _realEstate.Area = int.Parse(AreaFromTextBox.Text);
            _realEstate.Rooms = int.Parse(RoomsFromTextBox.Text);
            _realEstate.Floor = int.Parse(FloorFromTextBox.Text);
            _realEstate.HeatingType = EnumHelper.ConvertToHeatingType((string)((ComboBoxItem)HeatingTypeComboBox.SelectedItem).Content);
            _realEstate.HasTerrace = TerraceCheckBox.IsChecked.Value;
            _realEstate.IsRegistered = RegisteredCheckBox.IsChecked.Value;
            this.Hide();
        }
    }
}
