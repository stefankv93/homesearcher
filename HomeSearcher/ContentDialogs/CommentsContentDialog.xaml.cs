﻿using HomeSearcher.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.ContentDialogs
{
    public sealed partial class CommentsContentDialog : ContentDialog, INotifyPropertyChanged
    {
        private RealEstate _realEstate;

        public RealEstate RealEstate
        {
            get { return _realEstate; }
            set
            {
                if (_realEstate != value)
                {
                    _realEstate = value;
                    NotifyPropertyChanged(nameof(RealEstate));
                }
            }
        }

        public CommentsContentDialog(RealEstate realEstate)
        {
            this.InitializeComponent();
            this.RealEstate = realEstate;

            this.DataContext = this;
        }

        /// <summary>
        /// Notifies that the property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
