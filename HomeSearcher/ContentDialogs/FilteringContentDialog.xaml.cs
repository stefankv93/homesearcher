﻿using HomeSearcher.Models;
using HomeSearcher.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace HomeSearcher.ContentDialogs
{
    public sealed partial class FilteringContentDialog : ContentDialog
    {
        private List<RealEstate> _estateList;
        public List<RealEstate> EstateListResult
        {
            get { return _estateList; }
        }

        public FilteringContentDialog(List<RealEstate> _estateList)
        {
            this.InitializeComponent();

            this._estateList = _estateList.ToList();
            this.FilterButton.Click += FilterButton_Click;
            this.CancelButton.Click += CancelButton_Click;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            RealEstate.EstateType estateType = EnumHelper.ConvertToEstateType((string)((ComboBoxItem)EstateTypeComboBox.SelectedValue).Content);

            if(estateType != RealEstate.EstateType.None)
            {
                _estateList = _estateList.Where((estate) => estate.Type == estateType).ToList();
            }

            int priceFrom = 0;
            int priceTo = int.MaxValue;

            if(!string.IsNullOrEmpty(PriceFromTextBox.Text))
            {
                if(!int.TryParse(PriceFromTextBox.Text, out priceFrom))
                {
                    //show error dialog
                }
            }

            if (!string.IsNullOrEmpty(PriceToTextBox.Text))
            {
                if (!int.TryParse(PriceToTextBox.Text, out priceTo))
                {
                    //show error dialog
                }
            }

            _estateList = _estateList.Where((estate) => estate.Price >= priceFrom && estate.Price <= priceTo).ToList();

            int areaFrom = 0;
            int areaTo = int.MaxValue;

            if (!string.IsNullOrEmpty(AreaFromTextBox.Text))
            {
                if (!int.TryParse(AreaFromTextBox.Text, out areaFrom))
                {
                    //show error dialog
                }
            }

            if (!string.IsNullOrEmpty(AreaToTextBox.Text))
            {
                if (!int.TryParse(AreaToTextBox.Text, out areaTo))
                {
                    //show error dialog
                }
            }

            _estateList = _estateList.Where((estate) => estate.Area >= areaFrom && estate.Area <= areaTo).ToList();

            int roomsFrom = 0;
            int roomsTo = int.MaxValue;

            if (!string.IsNullOrEmpty(RoomsFromTextBox.Text))
            {
                if (!int.TryParse(RoomsFromTextBox.Text, out roomsFrom))
                {
                    //show error dialog
                }
            }

            if (!string.IsNullOrEmpty(RommsToTextBox.Text))
            {
                if (!int.TryParse(RommsToTextBox.Text, out roomsTo))
                {
                    //show error dialog
                }
            }

            _estateList = _estateList.Where((estate) => estate.Rooms >= roomsFrom && estate.Rooms <= roomsTo).ToList();

            int floorsFrom = 0;
            int floorsTo = int.MaxValue;

            if (!string.IsNullOrEmpty(FloorFromTextBox.Text))
            {
                if (!int.TryParse(FloorFromTextBox.Text, out floorsFrom))
                {
                    //show error dialog
                }
            }

            if (!string.IsNullOrEmpty(FloorToTextBox.Text))
            {
                if (!int.TryParse(FloorToTextBox.Text, out floorsTo))
                {
                    //show error dialog
                }
            }

            _estateList = _estateList.Where((estate) => estate.Floor >= floorsFrom && estate.Floor <= floorsTo).ToList();

            RealEstate.Heating heatingFilter = EnumHelper.ConvertToHeatingType((string)((ComboBoxItem)HeatingTypeComboBox.SelectedItem).Content);

            if(heatingFilter != RealEstate.Heating.None)
            {
                _estateList = _estateList.Where((estate) => estate.HeatingType == heatingFilter).ToList();
            }

            if(TerraceCheckBox.IsChecked.Value)
            {
                _estateList = _estateList.Where((estate) => estate.HasTerrace == true).ToList();
            }

            if(RegisteredCheckBox.IsChecked.Value)
            {
                _estateList = _estateList.Where((estate) => estate.IsRegistered == true).ToList();
            }

            DateTime dateFrom = DateTime.MinValue;
            DateTime dateTo = DateTime.MaxValue;

            if(CalendarFrom.Date.HasValue)
            {
                dateFrom = CalendarFrom.Date.Value.Date;
            }

            if(CalendarTo.Date.HasValue)
            {
                dateTo = CalendarTo.Date.Value.Date;
            }

            _estateList = _estateList.Where((estate) => estate.DateCreation >= dateFrom && estate.DateCreation <= dateTo).ToList();


            this.Hide();
        }
    }
}
